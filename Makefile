.PHONY : flask venv run

run : 
	@echo "1st step run make venv"
	@echo "2nd step run source venv/bin/activate"
	@echo "3rd step  make flask"
venv :
	@virtualenv -p python3 venv
	@echo "now you need to the 2nd step"

flask:
	@pip install flask
	@pip install flask-sqlalchemy
	@pip install flask-wtf
	@pip install python-dotenv
	@pip install pyyaml
	@pip install flask-bootstrap4
	@pip install click
	@echo "congratulation now you can run flask with this virtual environment"
