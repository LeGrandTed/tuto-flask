from .app import app,db
from flask import render_template,request,redirect,url_for
import yaml, os.path
from .models import get_sample,get_books_of_author,get_authors,get_books,get_book,get_author

@app.route("/")
def home():
    return render_template("home.html",title = "Tiny Amazon",books=get_sample())

@app.route("/Authors")
def test():
	return render_template("authors.html",title = "Tiny Amazon",authors=get_authors())
	
'''
@app.route("/Authors/book/<int:id>")
def booksofA(id):
	return render_template("home.html",title = "Tiny Amazon",books=get_books_of_author(id))
'''
@app.route("/Authors/<int:id>")
def author(id):
	return render_template("author.html",title = get_author(id).name,author=get_author(id),books=get_books_of_author(id))

@app.route("/Books")
def books():
    return render_template("home.html",title = "Tiny Amazon",books=get_books())

@app.route("/Books/<int:id>")
def booksbyid(id):
    return render_template("booksById.html",title = "Tiny Amazon",book=get_book(id))

from flask_wtf import FlaskForm
from wtforms import StringField,HiddenField
from wtforms.validators import DataRequired

class AuthorForm(FlaskForm):
    id = HiddenField('id')
    name = StringField('Nom',validators=[DataRequired()])

@app.route("/edit/author/<int:id>")
def edit_author(id):
    a = None
    a = get_author(id)
    f = AuthorForm(id=a.id,name=a.name)
    return render_template(
        "edit_author.html",
        author=a,form=f,title = "edit %s"%a.name
    )

@app.route("/save/author",methods=["POST"])
def save_author():
    a = None
    f = AuthorForm()
    if f.validate_on_submit():
        a = get_author(int(f.id.data))
        a.name = f.name.data
        db.session.commit()
        return redirect(url_for('author',id=a.id))
    get_author(int(f.id.data))
    return redirect(url_for('edit_author',id=a.id))