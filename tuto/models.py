#import yaml,os.path

#Books = yaml.load(open(os.path.join(os.path.dirname(__file__),"data.yml")),Loader=yaml.SafeLoader)

#def get_sample():
#	return Books[0:10]
from .app import db

class Author(db.Model):
    #__tablename__ = "author"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(100))
    def __repr__(self):
    	return "<Author (%d) %s>" % (self.id, self.name)

class Book(db.Model):
    #__tablename__ = "book"
    id = db.Column(db.Integer, primary_key = True)
    img = db.Column(db.String(100))
    title = db.Column(db.String(200))
    price = db.Column(db.Float)
    url = db.Column(db.String(200))
    author_id = db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship("Author", backref=db.backref("books", lazy="dynamic"))
    def __repr__(self):
    	return "<Book (%d) %s>" % (self.id, self.title)

def get_sample():
    return Book.query.limit(10).all()

def get_book(id):
    return Book.query.get_or_404(id)

def get_books():
    return Book.query.all()

def get_authors():
    return Author.query.all()

def get_author(id):
    return Author.query.get_or_404(id)

def get_books_of_author(id):
    return get_author(id).books.all()